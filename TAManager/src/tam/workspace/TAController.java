package tam.workspace;

import djf.AppTemplate;
import djf.controller.AppFileController;
import static tam.TAManagerProp.*;
import djf.ui.AppMessageDialogSingleton;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.workspace.TAWorkspace;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file
 * toolbar.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public class TAController{
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;

    /**
     * Constructor, note that the app must already be constructed.
     */
    public TAController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }
    
    /**
     * This method responds to when the user requests to add
     * a new TA via the UI. Note that it must first do some
     * validation to make sure a unique name and email address
     * has been provided.
     */
    public void handleAddTA() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();//*********************
        String name = nameTextField.getText();
        String email = emailTextField.getText();//*************************
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData)app.getDataComponent();
        
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (name.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));            
        }
        //**************************
        if (email.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));            
        }
        // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsTA(name)) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));                                    
        }
        // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            
            
            //app.getGUI().updateToolbarControls(false);
            
            data.addTA(name,email);
            
            // CLEAR THE TEXT FIELDS
            nameTextField.setText("");
            
            //**************************************
            emailTextField.setText("");
            
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
            app.getGUI().getMarkAsEdited();
            
            
        }
    }

    /**
     * This function provides a response for when the user clicks
     * on the office hours grid to add or remove a TA to a time slot.
     * 
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane) {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        
        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        if(selectedItem!=null){
        // GET THE TA
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        String taName = ta.getName();
        
        TAData data = (TAData)app.getDataComponent();
        String cellKey = pane.getId();
        
        StringProperty prop = data.getOfficeHours().get(cellKey);
        
        // AND TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
        if(prop.getValue().contains(taName))
            data.removeTAFromCell(prop, taName);
        else
            data.toggleTAOfficeHours(cellKey, taName);
        
        app.getGUI().getMarkAsEdited();
        }
    }
   
    public void handleMouseEnter(Pane pane){
        pane.setStyle( "-fx-border-color:yellow;-fx-border-width:2px;");
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();
        String cellKey=workspace.getCellKey(pane);
        String[] col_row = cellKey.split("_");
        String col= col_row[0];
        String row= col_row[1];
        int c= Integer.parseInt(col);
        int r= Integer.parseInt(row);
        //change rows
        for(int i=1; i<r; i++){
            String ck = ""+c+"_"+i;
            Pane p=(Pane)workspace.getOfficeHoursGridTACellPanes().get(ck);
            p.setStyle( "-fx-border-color:#f7f6cd;-fx-border-width:2px;");
        }
        
         //change cols
        for(int j=2; j<c; j++){
            String ck2= ""+j+"_"+r;
            Pane p=(Pane)workspace.getOfficeHoursGridTACellPanes().get(ck2);
            p.setStyle( "-fx-border-color:#f7f6cd;-fx-border-width:2px;");   
        }
         // day header
         String ckDay=""+c+"_"+0;
         Pane p1 = (Pane)workspace.getOfficeHoursGridDayHeaderPanes().get(ckDay);
         p1.setStyle( "-fx-border-color:#f7f6cd;-fx-border-width:2px;");
         // time, 2nd pane
         String ckTime =""+1+"_"+r;
         Pane p2 = (Pane)workspace.getOfficeHoursGridTimeCellPanes().get(ckTime);
         p2.setStyle( "-fx-border-color:#f7f6cd;-fx-border-width:2px;");
         // time, 1st pane
         String ckTime2=""+0+"_"+r;
         Pane p3 = (Pane)workspace.getOfficeHoursGridTimeCellPanes().get(ckTime2);
         p3.setStyle( "-fx-border-color:#f7f6cd;-fx-border-width:2px;");
         
    }
    public void handleMouseExit(Pane pane){
        pane.setStyle( "-fx-border-color:#000000;-fx-border-width:1px;");
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();
        String cellKey=workspace.getCellKey(pane);
        String[] col_row = cellKey.split("_");
        String col= col_row[0];
        String row= col_row[1];
        int c= Integer.parseInt(col);
        int r= Integer.parseInt(row);
        
        //change rows
        for(int i=1; i<r; i++){
            String ck = ""+c+"_"+i;
            Pane p=(Pane)workspace.getOfficeHoursGridTACellPanes().get(ck);
            p.setStyle( "-fx-border-color:#000000;-fx-border-width:1px;");//#b2b174
        }   
        // change cols 
        for(int j=2; j<c; j++){
            String ck2= ""+j+"_"+r;
            //HashMap map=workspace.getOfficeHoursGridTACellPanes();
            Pane p=(Pane)workspace.getOfficeHoursGridTACellPanes().get(ck2);
            p.setStyle( "-fx-border-color:#000000;-fx-border-width:1px;");
        }
         //day header exit
         String ckDay=""+c+"_"+0;
         Pane p1 = (Pane)workspace.getOfficeHoursGridDayHeaderPanes().get(ckDay);
         p1.setStyle( "-fx-border-color:#15056d;");
         // time, 2nd pane exit
         String ckTime =""+1+"_"+r;
         Pane p2 = (Pane)workspace.getOfficeHoursGridTimeCellPanes().get(ckTime);
         p2.setStyle( "-fx-background-color: #2308b2;-fx-border-color: white;-fx-border-style:dotted;-fx-border-width: 1px;");
         // time, 1st pane exit
         String ckTime2=""+0+"_"+r;
         Pane p3 = (Pane)workspace.getOfficeHoursGridTimeCellPanes().get(ckTime2);
         p3.setStyle( "-fx-background-color: #2308b2;-fx-border-color: white;-fx-border-style:dotted;-fx-border-width: 1px;");
        
    }
    
    
    public void handleDeleteKey(KeyEvent e){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        TAData data = (TAData) app.getDataComponent();
        ObservableList<TeachingAssistant> tableData = data.getTeachingAssistants();
        
            Object selectedItem = taTable.getSelectionModel().getSelectedItem();
            TeachingAssistant ta = (TeachingAssistant)selectedItem;
            String taName=null;
            if(selectedItem!=null)
            taName = ta.getName();
            
            if(selectedItem!=null&&!tableData.isEmpty()){
                if(e.getCode().equals(KeyCode.DELETE)){
                    tableData.remove(ta); 
                    
                     for(String cellKey: data.getOfficeHours().keySet() ){
                            StringProperty prop = data.getOfficeHours().get(cellKey); 
                            if(prop.getValue().contains(taName))
                            data.removeTAFromCell(prop, taName);
                        }
                     
                 app.getGUI().getMarkAsEdited();    
              }
             
                       
                    
            }
            }
        };
    
